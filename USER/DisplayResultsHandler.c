#include "DisplayResultsHandler.h"

#include "TiBoardTftHandler.h"
#include <stdio.h>
#include <stdint.h>

#define DISTANCE_TO_BORDER_ADDRESS 0
#define DISTANCE_TO_BORDER_CURRENT_ACTION_MARKER ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT
#define ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT 2
#define DISTANCE_TO_BORDER_TEMP (ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT + 1)
#define TEMPERATURE_WIDTH 7
#define WIDTH_OF_ENTRY (DISTANCE_TO_BORDER_TEMP + TEMPERATURE_WIDTH)
#define ENTRIES_PER_COLUMN (TIBOARD_TFT_WIDTH / WIDTH_OF_ENTRY)

void DisplayResultsInitDisplay(void) {
    TiBoardTftInit();
    //TiBoardTftWriteStringToDisplayLine(1, "Anybody out there?", 0);
}

void GetLineAndPositionFromEntryNumber(int entry, int* line, int* position) {
    //entry 0 basiert
    //line =y
    //position =x
    int entriesPerColumn =

    *line += entry / ENTRIES_PER_COLUMN;
    *position += (entry % ENTRIES_PER_COLUMN) * WIDTH_OF_ENTRY;

    //1 basiert
    *line += 1;
    *position += 1;
}

void ClearEntryOnDislay(int entry)
{
    char tmpString[WIDTH_OF_ENTRY];
    int position = 0;
    int line = 0;
    int i;
    
    GetLineAndPositionFromEntryNumber(entry, &line, &position);
    
    for (i = 0; i < WIDTH_OF_ENTRY; i++)
        tmpString[i] = ' ';
    
    TiBoardTftWriteStringToDisplayPosition(position, line, WIDTH_OF_ENTRY, tmpString, TIBOARD_TFT_TEXTSTYLE_TEXT);
}

void DisplayResultsSetEntryAddress(int entry, uint64_t address, int connected) {
    int i;
    char tmpString[2];
    int line = 0;
    int position = DISTANCE_TO_BORDER_ADDRESS;
    int tmpAddressPart;

    GetLineAndPositionFromEntryNumber(entry, &line, &position);

    for (i = 0; i < ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT / 2; i++) {
        tmpAddressPart = (address & (255LL << ((i + (16 - ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT) / 2)*8))) >> ((i + (16 - ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT) / 2)*8);
        sprintf(tmpString, "%02x", tmpAddressPart);
        TiBoardTftWriteStringToDisplayPosition(position + (ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT - 2 * (i + 1)), line, 2, tmpString, connected ? TIBOARD_TFT_TEXTSTYLE_INFO : TIBOARD_TFT_TEXTSTYLE_WARNING);
    }
}

void DisplayResultsAddressList(ADDRESSLIST *addressList, int *lastNumberOfEntries) {
    ADDRESSLIST* currentAddressListEntry = addressList;
    int currentAddressIndex = 0;
    int connectedDevices = 0;
    char tmpString[25];

    while (currentAddressListEntry) {
        DisplayResultsSetEntryAddress(currentAddressIndex, currentAddressListEntry->address, currentAddressListEntry->connected);

        if (currentAddressListEntry->connected)
            connectedDevices++;

        currentAddressIndex++;
        currentAddressListEntry = currentAddressListEntry->next;
    }

    sprintf(tmpString, "%02d devices connected", connectedDevices);
    TiBoardTftWriteStringToDisplayPosition(9, 0, 25, tmpString, TIBOARD_TFT_TEXTSTYLE_INFO);
    
    while (currentAddressIndex < *lastNumberOfEntries)
    {
        ClearEntryOnDislay(currentAddressIndex++);
    }
    *lastNumberOfEntries = currentAddressIndex;
}

void DisplayResultsSetEntryMarker(int entry, int enabled) {
    int line = 0;
    int position = DISTANCE_TO_BORDER_CURRENT_ACTION_MARKER;

    GetLineAndPositionFromEntryNumber(entry, &line, &position);

    TiBoardTftWriteStringToDisplayPosition(position, line, 1, enabled ? ">" : " ", TIBOARD_TFT_TEXTSTYLE_TEXT);
    //TiBoardTftWriteStringToDisplayPosition(position + ADDRESS_DISPLAY_AMOUNT_OF_HEX_DIGIT, line, 1, enabled ? ">" : " ", TIBOARD_TFT_TEXTSTYLE_TEXT);
}

void DisplayResultsSetEntryTemperature(int entry, float temperature, int kelvin) {
    char tmpString[TEMPERATURE_WIDTH];
    int line = 0;
    int position = DISTANCE_TO_BORDER_TEMP - 1;

    GetLineAndPositionFromEntryNumber(entry, &line, &position);

    if (temperature == 85.0f) {
        sprintf(tmpString, " ERROR  ");
        TiBoardTftWriteStringToDisplayPosition(position, line, TEMPERATURE_WIDTH, tmpString, TIBOARD_TFT_TEXTSTYLE_WARNING);
    } else {
        if (kelvin)
            sprintf(tmpString, " %04.1fK ", temperature + 273.15f);
        else
            sprintf(tmpString, " %04.1fC ", temperature);

        TiBoardTftWriteStringToDisplayPosition(position, line, TEMPERATURE_WIDTH, tmpString, TIBOARD_TFT_TEXTSTYLE_TEXT);
    }
}
