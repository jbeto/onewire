#include "OneWireHandler.h"

void DisplayResultsInitDisplay(void);
void DisplayResultsAddressList(ADDRESSLIST *addressList, int *lastNumberOfEntries);
void DisplayResultsSetEntryMarker(int entry, int enabled);
void DisplayResultsSetEntryTemperature(int entry, float temperature, int kelvin);
