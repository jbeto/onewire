#include "OneWireHandler.h"

#include <stdlib.h>
#include "TiBoardHandler.h"

#define DELETE_ADDRESS_LIST_FOREACH_ROMSEARCH 0

// all wait times in µs
#define BIT_SEND_TIME_OF_COMMAND (70 - 1)
#define BIT_SEND_ZERO_TIME_AFTER_START_TO_SET_HIGH (60 - 1)
#define BIT_SEND_ONE_TIME_AFTER_START_TO_SET_HIGH 6
#define BIT_SEND_MEASURE_DELAY_AFTER_CHANGE 9

#define RESET_SEND_TIME_OF_COMMAND 960
#define RESET_SEND_TIME_AFTER_START_TO_SET_HIGH 480
#define RESET_SEND_MEASURE_DELAY_AFTER_CHANGE 70

//int OneWireSendOneWithReadIn(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
//    int readIn = 1;
//    int i;
//
//    TiBoardSetOpenDrainOfMultiplePins(numberOfPins, tiBoardPins, 1);
//    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 0);
//    TiBoardWaitMicroSecs(6);
//    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 1);
//    TiBoardWaitMicroSecs(9);
//    for (i = 0; i < numberOfPins; i++) {
//        if (TiBoardGetPinState(tiBoardPins[i]) == 0)
//            readIn = 0;
//    }
//    TiBoardWaitMicroSecs(55 - 1);
//
//    return readIn;
//}
//
//void OneWireSendZero(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
//    TiBoardSetOpenDrainOfMultiplePins(numberOfPins, tiBoardPins, 1);
//    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 0);
//    TiBoardWaitMicroSecs(60 - 1);
//    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 1);
//    TiBoardWaitMicroSecs(10);
//}

int OneWireSendBitWithReadIn(int numberOfPins, TIBOARDPIN tiBoardPins[], int bitToSend) {
    int readIn = 1;
    int i;
    int endTime = TiBoardGetValueOfTimerInMicroSecs(BIT_SEND_TIME_OF_COMMAND);

    TiBoardSetOpenDrainOfMultiplePins(numberOfPins, tiBoardPins, 1);
    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 0);
    TiBoardWaitMicroSecs(bitToSend ? BIT_SEND_ONE_TIME_AFTER_START_TO_SET_HIGH : BIT_SEND_ZERO_TIME_AFTER_START_TO_SET_HIGH);
    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 1);
    TiBoardWaitMicroSecs(BIT_SEND_MEASURE_DELAY_AFTER_CHANGE);

    for (i = 0; i < numberOfPins; i++) {
        if (TiBoardGetPinState(tiBoardPins[i]) == 0)
            readIn = 0;
    }

    TiBoardWaitUntilDestinationTimerValue(endTime);
    return readIn;
}

void OneWireSendBit(int numberOfPins, TIBOARDPIN tiBoardPins[], int bitToSend) {
    //    if (bitToSend)
    //        OneWireSendOneWithReadIn(numberOfPins, tiBoardPins);
    //    else
    //        OneWireSendZero(numberOfPins, tiBoardPins);

    OneWireSendBitWithReadIn(numberOfPins, tiBoardPins, bitToSend);
}

int OneWireReceiveBit(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    return OneWireSendBitWithReadIn(numberOfPins, tiBoardPins, 1);
}

void OneWireSendPower(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    int i;

    for (i = 0; i < numberOfPins; i++)
        TiBoardSetOpenDrainOfPin(tiBoardPins[i], 0);
}

int OneWireSendReset(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    int readIn = 1;
    int i;
    int endTime = TiBoardGetValueOfTimerInMicroSecs(RESET_SEND_TIME_OF_COMMAND);

    TiBoardSetOpenDrainOfMultiplePins(numberOfPins, tiBoardPins, 1);
    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 0);
    TiBoardWaitMicroSecs(RESET_SEND_TIME_AFTER_START_TO_SET_HIGH);
    TiBoardSetMultiplePinState(numberOfPins, tiBoardPins, 1);
    TiBoardWaitMicroSecs(RESET_SEND_MEASURE_DELAY_AFTER_CHANGE);
    for (i = 0; i < numberOfPins; i++) {
        if (TiBoardGetPinState(tiBoardPins[i]) == 0)
            readIn = 0;
    }

    TiBoardWaitUntilDestinationTimerValue(endTime);
    return (~readIn & 1 << 0);
}

void OneWireSendByte(int numberOfPins, TIBOARDPIN tiBoardPins[], uint8_t byteToSend) {
    int i;

    for (i = 0; i < 8; i++) {
        OneWireSendBit(numberOfPins, tiBoardPins, byteToSend & (1 << i));
    }
}

uint8_t OneWireReceiveByte(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    uint8_t receivedByte = 0;
    int i;

    for (i = 0; i < 8; i++) {
        receivedByte |= OneWireReceiveBit(numberOfPins, tiBoardPins) << i;
    }

    return receivedByte;
}

//uint8_t GenerateCRC(uint64_t address)
//{
//    uint8_t crc = 0;
//    uint8_t currentByte;
//    int i;
//    int j;
//    
//    for (i = 0; i < 8; i++)
//    {
//        currentByte = (address & (0xFFLL << (i)*8)) >> (i)*8;
//        for (j = 0; j < 8; j++)
//        {
//            crc >>= 1;
//            if ((crc ^ currentByte) & 0x01)
//                crc ^= 0x8C;            
//        }
//    }
//    
//    return crc;
//}

int OneWireCheckCRC(uint64_t address)
{
    return 1;
}

void OneWireReadROMOfSingleDevice(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t *returnAddress) {
    int i;
    *returnAddress = 0;

    OneWireSendReset(numberOfPins, tiBoardPins);
    OneWireSendByte(numberOfPins, tiBoardPins, 0x33);

    for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++) {
        *returnAddress &= (uint64_t) OneWireReceiveBit(numberOfPins, tiBoardPins) << i;
    }
}

void OneWireSendROMSkip(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    OneWireSendByte(numberOfPins, tiBoardPins, 0xCC);
}

//int AddressEquals(uint8_t address1[ONEWIRE_ROM_BIT_LENGTH], uint8_t address2[ONEWIRE_ROM_BIT_LENGTH]) {
//    int i;
//    for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++) {
//        if (address1[i] != address2[i])
//            return 0;
//    }

//    return 1;
//}

void AddressListSetAllDisconnected(ADDRESSLIST** list, int deleteFullList) {
    ADDRESSLIST* currentAddressListEntry = *list;
    ADDRESSLIST* lastEntry = 0;

    if (deleteFullList)
    {
        while (*list)
        {
            while (currentAddressListEntry->next)
            {
                lastEntry = currentAddressListEntry;
                currentAddressListEntry = currentAddressListEntry->next;
            }
            
            free(currentAddressListEntry);
            if (lastEntry)
                lastEntry->next = 0;
            else
                *list = 0;
            
            currentAddressListEntry = *list;
            lastEntry = 0;
        }
    }
    else
    {
        while (currentAddressListEntry) {
            currentAddressListEntry->connected = 0;
            currentAddressListEntry = currentAddressListEntry->next;
        }
    }
}

void AddressListAddDevice(ADDRESSLIST** list, uint64_t addressFound) {
    ADDRESSLIST* currentAddressListEntry = *list;
    ADDRESSLIST* lastAddressListEntry = 0;
    //int i;

    while (currentAddressListEntry) {
        if (addressFound == currentAddressListEntry->address) {
            //Bereits in der Liste
            currentAddressListEntry->connected = 1;
            return;
        }

        lastAddressListEntry = currentAddressListEntry;
        currentAddressListEntry = currentAddressListEntry->next;
    }

    currentAddressListEntry = 0;
    currentAddressListEntry = (ADDRESSLIST*) malloc(sizeof (ADDRESSLIST));
    if (currentAddressListEntry == 0)
        return; //malloc panik

    //for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++)
    currentAddressListEntry->address = addressFound;
    currentAddressListEntry->connected = 1;
    currentAddressListEntry->next = 0;

    if (lastAddressListEntry != 0)
        lastAddressListEntry->next = currentAddressListEntry;
    else
        *list = currentAddressListEntry;
}

int OneWireROMSeachSpecific(int numberOfPins, TIBOARDPIN tiBoardPins[], ADDRESSLIST **addressList, uint64_t *lastAddress, int *lastConflictPosition) {
    int currentBitIndex = 0;
    uint64_t address = 0LL;
    int sensorFound = 1;
    int currentLastConflictPosition = -1;
    int receiveTwoBits;
    int currentBit;
    //int i;

    if (!OneWireSendReset(numberOfPins, tiBoardPins)){
        // Keiner da
        sensorFound = 0;
    }
    else
    {
        OneWireSendByte(numberOfPins, tiBoardPins, 0xF0);

        for (currentBitIndex = 0; currentBitIndex < 64; currentBitIndex++) {
            receiveTwoBits = OneWireReceiveBit(numberOfPins, tiBoardPins) << 0;
            receiveTwoBits |= OneWireReceiveBit(numberOfPins, tiBoardPins) << 1;

            if (receiveTwoBits == 3) {
                // Keiner da
                sensorFound = 0;
                break;
            }

            if (receiveTwoBits == 1 || receiveTwoBits == 2) {
                //Eindeutige Adresse das jeweilige Bit nehmen
                if (receiveTwoBits == 1)
                    currentBit = 1;
                else
                    currentBit = 0;
            } else //if (receiveTwoBits == 0)
            {
                // Gibt Unterschiedliche Bits
                if (currentBitIndex == *lastConflictPosition) {
                    currentBit = 1;
                } else if (currentBitIndex < *lastConflictPosition && *lastAddress & (1LL << currentBitIndex)) {
                    //Konflikt bereits behandelt
                    currentBit = 1;
                } else {
                    currentBit = 0;
                    currentLastConflictPosition = currentBitIndex;
                }
            }

            address &= ~(1LL << currentBitIndex);
            address |= (uint64_t) currentBit << currentBitIndex;

            OneWireSendBit(numberOfPins, tiBoardPins, currentBit);
        }
    }

    *lastConflictPosition = currentLastConflictPosition;
    //for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++)
    *lastAddress = address;

    if (addressList) {
        if (OneWireCheckCRC(address)) {
            if (OneWireIsDeviceConnected(numberOfPins, tiBoardPins, address)) {
                AddressListAddDevice(addressList, address);
                sensorFound = 1;
            } else
                sensorFound = 0;
        }
        else
            sensorFound = 0;
    } else
        sensorFound = 1;
    
    return sensorFound;
}

void OneWireROMSeach(int numberOfPins, TIBOARDPIN tiBoardPins[], ADDRESSLIST **addressList) {
    int lastConflictPosition = 64;
    uint64_t currentAddress = 0LL;

    AddressListSetAllDisconnected(addressList, DELETE_ADDRESS_LIST_FOREACH_ROMSEARCH);

    while (lastConflictPosition >= 0) {
        OneWireROMSeachSpecific(numberOfPins, tiBoardPins, addressList, &currentAddress, &lastConflictPosition);
    }
}

int OneWireIsDeviceConnected(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address) {
    int currentBitIndex = 0;
    int receiveTwoBits;
    int currentBit;

    OneWireSendReset(numberOfPins, tiBoardPins);
    OneWireSendByte(numberOfPins, tiBoardPins, 0xF0);

    for (currentBitIndex = 0; currentBitIndex < 64; currentBitIndex++) {
        receiveTwoBits = OneWireReceiveBit(numberOfPins, tiBoardPins) << 0;
        receiveTwoBits |= OneWireReceiveBit(numberOfPins, tiBoardPins) << 1;

        if (receiveTwoBits == 3) {
            // Keiner da
            return 0;
        }
        currentBit = (address & (1LL << currentBitIndex)) > 0;
        OneWireSendBit(numberOfPins, tiBoardPins, currentBit);
    }
    return 1;
}

void OneWireSendROMSelect(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address) {
    int i;
    uint8_t currentByteToSend;

    OneWireSendByte(numberOfPins, tiBoardPins, 0x55);

    for (i = 0; i < ONEWIRE_ROM_BIT_LENGTH; i++) {
        currentByteToSend = (address & (0xFFLL << i*8)) >> i*8;
                
        OneWireSendByte(numberOfPins, tiBoardPins, currentByteToSend);
    }
}
