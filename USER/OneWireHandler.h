#include <stdint.h>

#include "TiBoardHandler.h"

//Verhindert Mehrfachdefinition
#ifndef ONEWIRE_ROM_BIT_LENGTH
#define ONEWIRE_ROM_BIT_LENGTH 8

typedef struct tagADDRESSLIST {
    uint64_t address;
    int connected;
    struct tagADDRESSLIST * next;
} ADDRESSLIST;
#endif

//int OneWireSendOneWithReadIn(int numberOfPins, TIBOARDPIN tiBoardPins[]);
//void OneWireSendZero(int numberOfPins, TIBOARDPIN tiBoardPins[]);
void OneWireSendBit(int numberOfPins, TIBOARDPIN tiBoardPins[], int bitToSend);
int OneWireReceiveBit(int numberOfPins, TIBOARDPIN tiBoardPins[]);

void OneWireSendPower(int numberOfPins, TIBOARDPIN tiBoardPins[]);

int OneWireSendReset(int numberOfPins, TIBOARDPIN tiBoardPins[]);

void OneWireSendByte(int numberOfPins, TIBOARDPIN tiBoardPins[], uint8_t byteToSend);
uint8_t OneWireReceiveByte(int numberOfPins, TIBOARDPIN tiBoardPins[]);

int OneWireCheckCRC(uint64_t address);
void OneWireReadROMOfSingleDevice(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t *returnAddress);
void OneWireSendROMSkip(int numberOfPins, TIBOARDPIN tiBoardPins[]);
void OneWireROMSeach(int numberOfPins, TIBOARDPIN tiBoardPins[], ADDRESSLIST **addressList);
int OneWireIsDeviceConnected(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address);
void OneWireSendROMSelect(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address);
