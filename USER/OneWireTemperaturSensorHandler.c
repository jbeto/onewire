#include "OneWireTemperaturSensorHandler.h"

#include "OneWireHandler.h"
#include "TiBoardHandler.h"

#define TEMPERATURE_SENSOR_DATA_BYTES 9

float GetTemperatureOfReturnedData(uint8_t returnedBytes[TEMPERATURE_SENSOR_DATA_BYTES]) {
    int16_t tmp = returnedBytes[1];
    tmp <<= 8;
    tmp |= returnedBytes[0];

    //	if (tmp & (1<<11))
    //      tmp |= 0xF000;

    return tmp / 16.0;
}

float ReadTemperatureFromBus(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    uint8_t data[TEMPERATURE_SENSOR_DATA_BYTES];
    int i;

    //Sende Befehl zum Temperatur senden
    OneWireSendByte(numberOfPins, tiBoardPins, 0xBE);

    //Lese Daten
    for (i = 0; i < TEMPERATURE_SENSOR_DATA_BYTES; i++) {
        data[i] = OneWireReceiveByte(numberOfPins, tiBoardPins);
    }

    //Get Temperature from Data
    return GetTemperatureOfReturnedData(data);
}

void SendTemperatureReadCommand(int numberOfPins, TIBOARDPIN tiBoardPins[]) {
    //Sende Temperatur Lese Befehl (0x44)
    OneWireSendByte(numberOfPins, tiBoardPins, 0x44);

    //Gib Saft
    OneWireSendPower(numberOfPins, tiBoardPins);

    //Warten...
    TiBoardWaitMilliSecs(650);
}

int OneWireTemperatureSensorGetTemperatureOfSingleConnectedSensor(int numberOfPins, TIBOARDPIN tiBoardPins[], float* temperature) {
    OneWireSendReset(numberOfPins, tiBoardPins);
    OneWireSendROMSkip(numberOfPins, tiBoardPins);
    SendTemperatureReadCommand(numberOfPins, tiBoardPins);

    if (!OneWireSendReset(numberOfPins, tiBoardPins))
        return 0;

    OneWireSendROMSkip(numberOfPins, tiBoardPins);
    *temperature = ReadTemperatureFromBus(numberOfPins, tiBoardPins);

    return 1;
}

int OneWireTemperatureSensorGetTemperatureOfSelectedSensor(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address, float* temperature) {
    OneWireSendReset(numberOfPins, tiBoardPins);
    OneWireSendROMSelect(numberOfPins, tiBoardPins, address);
    SendTemperatureReadCommand(numberOfPins, tiBoardPins);

    if (!OneWireIsDeviceConnected(numberOfPins, tiBoardPins, address))
        return 0;

    OneWireSendReset(numberOfPins, tiBoardPins);
    OneWireSendROMSelect(numberOfPins, tiBoardPins, address);
    *temperature = ReadTemperatureFromBus(numberOfPins, tiBoardPins);

    return 1;
}

int OneWireTemperatureSensorIsAddressOfSensor(uint64_t address) {
    return (address & 0xFF) == 0x28;
}
