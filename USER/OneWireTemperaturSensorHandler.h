#include <stdint.h>

#include "TiBoardHandler.h"
#include "OneWireHandler.h"

int OneWireTemperatureSensorGetTemperatureOfSingleConnectedSensor(int numberOfPins, TIBOARDPIN tiBoardPins[], float* temperature);
int OneWireTemperatureSensorGetTemperatureOfSelectedSensor(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address, float* temperature);
int OneWireTemperatureSensorIsAddressOfSensor(uint64_t address);
