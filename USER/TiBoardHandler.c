#include "TiBoardHandler.h"

//#include "..\stm32f4xx.h"
#include "..\stm32f4xx_gpio.h"

#define TIM2_CLOCK 84 //MHz

void TiBoardSetPinState(TIBOARDPIN tiBoardPin, int newState) {
    int newValue = tiBoardPin.port->ODR;

    if (newState)
        newValue |= (1 << tiBoardPin.pin);
    else
        newValue &= ~(1 << tiBoardPin.pin);

    tiBoardPin.port->ODR = newValue;
}

int TiBoardGetPinState(TIBOARDPIN tiBoardPin) {
    return (tiBoardPin.port->IDR & (1 << tiBoardPin.pin)) != 0;
}

void TiBoardSetMultiplePinState(int numberOfPins, TIBOARDPIN tiBoardPins[], int newState) {
    int i;
    for (i = 0; i < numberOfPins; i++)
        TiBoardSetPinState(tiBoardPins[i], newState);
}

void TiBoardSetOpenDrainOfPin(TIBOARDPIN tiBoardPin, int newState) {
    if (newState)
        tiBoardPin.port->OTYPER |= (1 << tiBoardPin.pin);
    else
        tiBoardPin.port->OTYPER &= ~(1 << tiBoardPin.pin);
}

void TiBoardSetOpenDrainOfMultiplePins(int numberOfPins, TIBOARDPIN tiBoardPins[], int newState) {
    int i;
    for (i = 0; i < numberOfPins; i++)
        TiBoardSetOpenDrainOfPin(tiBoardPins[i], newState);
}

void TiBoardInitTimer(void) {
    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    TIM2->CR1 = 0;
    TIM2->CR2 = 0;
    TIM2->PSC = 0;
    TIM2->ARR = 0xffffffff;
    TIM2->DIER = 0;
    TIM2->CR1 = TIM_CR1_CEN;
}

void TiBoardWaitMilliSecs(int milliSecsToWait) {
    TiBoardWaitMicroSecs(milliSecsToWait * 1000);
}

void TiBoardWaitMicroSecs(int microSecsToWait) {
    int t = TiBoardGetValueOfTimerInMicroSecs(microSecsToWait);

    TiBoardWaitUntilDestinationTimerValue(t);
}

int TiBoardGetValueOfTimerInMicroSecs(int deltaMicroSecs) {
    return TI_BOARD_TIMER + TIM2_CLOCK * deltaMicroSecs;
}

void TiBoardWaitUntilDestinationTimerValue(int timerEndValue) {
    while ((int) (TI_BOARD_TIMER - timerEndValue) < 0) {
    }
}
