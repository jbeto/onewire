#include "..\stm32f4xx.h"

//Verhindert Mehrfachdefinition
#ifndef DEFINE_TIBOARDPIN
#define DEFINE_TIBOARDPIN

typedef struct tagTIBOARDPIN {
    GPIO_TypeDef* port;
    int pin;
} TIBOARDPIN;

#endif

//Get / Set Pin
void TiBoardSetPinState(TIBOARDPIN tiBoardPin, int newState);
int TiBoardGetPinState(TIBOARDPIN tiBoardPin);

void TiBoardSetMultiplePinState(int numberOfPins, TIBOARDPIN tiBoardPins[], int newState);

//Open Drain / Push Pull of Pin
void TiBoardSetOpenDrainOfPin(TIBOARDPIN tiBoardPin, int newState);
void TiBoardSetOpenDrainOfMultiplePins(int numberOfPins, TIBOARDPIN tiBoardPins[], int newState);

//Timer
#define TI_BOARD_TIMER (TIM2->CNT)
void TiBoardInitTimer(void);
void TiBoardWaitMilliSecs(int milliSecsToWait);
void TiBoardWaitMicroSecs(int microSecsToWait);
int TiBoardGetValueOfTimerInMicroSecs(int deltaMicroSecs);
void TiBoardWaitUntilDestinationTimerValue(int timerEndValue);
