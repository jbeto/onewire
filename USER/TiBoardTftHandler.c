#include "TiBoardTftHandler.h"

#include "..\tft.h"
#include "..\stm32f4xx.h"
#include <stdio.h>

#include "OneWireHandler.h"

#define FONT_COLOR_HEADING 2
#define FONT_COLOR_TEXT 6
#define FONT_COLOR_WARNING 3
#define FONT_COLOR_INFO 7

void TiBoardTftInit(void) {
    TiBoardTftWriteStringToDisplayPosition(17,1, 8,"One Wire", TIBOARD_TFT_TEXTSTYLE_HEADING);
    TiBoardTftWriteStringToDisplayPosition(07,2,28,"Edgar Toll & Johannes Berger", TIBOARD_TFT_TEXTSTYLE_HEADING);
    
    TFT_cursor_off();

    TFT_set_window(2, 1, 4, TIBOARD_TFT_WIDTH, TIBOARD_TFT_HEIGHT + 1);
}

void TiBoardTftClear(void) {
    TFT_cls();
}

void TiBoardTftWriteStringToDisplayPosition(int x, int y, int length, char* string, int textStyle) {
    char output[TIBOARD_TFT_WIDTH + 1];
    int i;
    int endFound = 0;

    for (i = 0; i < length; i++) {
        if (endFound)
            output[i] = ' ';
        else {
            if (string[i] == 0) {
                endFound = 1;
                i--;
            } else
                output[i] = string[i];
        }
    }

    output[length] = 0;

    switch (textStyle)
    {
        case TIBOARD_TFT_TEXTSTYLE_HEADING:
            TFT_set_font_color(FONT_COLOR_HEADING);
            break;
        case TIBOARD_TFT_TEXTSTYLE_INFO:
            TFT_set_font_color(FONT_COLOR_INFO);
            break;
        case TIBOARD_TFT_TEXTSTYLE_WARNING:
            TFT_set_font_color(FONT_COLOR_WARNING);
            break;
        case TIBOARD_TFT_TEXTSTYLE_TEXT:
        default:
            TFT_set_font_color(FONT_COLOR_TEXT);
            break;        
    }
    
    TFT_gotoxy(x, y);
    TFT_puts(output);

    TFT_set_font_color(FONT_COLOR_TEXT);
}

void TiBoardTftWriteStringToDisplayLine(int y, char* string, int textStyle) {
    TiBoardTftWriteStringToDisplayPosition(1, y, TIBOARD_TFT_WIDTH, string, textStyle);
}
