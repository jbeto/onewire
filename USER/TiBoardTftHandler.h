#include "OneWireHandler.h"

#define TIBOARD_TFT_HEIGHT 12
#define TIBOARD_TFT_WIDTH 40

#define TIBOARD_TFT_TEXTSTYLE_HEADING 0
#define TIBOARD_TFT_TEXTSTYLE_TEXT 1
#define TIBOARD_TFT_TEXTSTYLE_INFO 2
#define TIBOARD_TFT_TEXTSTYLE_WARNING 3

void TiBoardTftInit(void);
void TiBoardTftClear(void);
void TiBoardTftWriteStringToDisplayPosition(int x, int y, int length, char* string, int textStyle);
void TiBoardTftWriteStringToDisplayLine(int y, char* string, int textStyle);
