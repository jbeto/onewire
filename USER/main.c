/**
 ******************************************************************************
 * @file    	main.c
 * @author  	Alfred Lohmann
 *        	  HAW-Hamburg
 *          	Labor f�r technische Informatik
 *          	Berliner Tor  7
 *          	D-20099 Hamburg
 * @version V1.0
 * @date    23.05.2013
 * @brief   Main program body
 *******************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "..\main.h"
#include "..\TI_Lib.h"
#include "..\tft.h"
/*
#include <stdio.h>
#include <string.h>
#include "..\stm32f4xx.h"
#include "..\stm32f4xx_gpio.h"
#include "..\stm32f4xx_rcc.h"
 */
#include "TiBoardHandler.h"
#include "OneWireHandler.h"
#include "OneWireTemperaturSensorHandler.h"
#include "DisplayResultsHandler.h"

/* Private typedef -----------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define DISPLAY_TEMPERATURE_AS_KELVIN 0
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void Init(void) {
    TIBOARDPIN pins[] = {
        {GPIOG, 5},
        {GPIOG, 6},
        {GPIOG, 7}
    };
    int numberOfPins = sizeof (pins) / sizeof (pins[0]);

    Init_TI_Board();
    DisplayResultsInitDisplay();
    TiBoardInitTimer();

    TiBoardSetMultiplePinState(numberOfPins, pins, 1);
}

void GetTemperaturOfSensorAndDisplay(int numberOfPins, TIBOARDPIN tiBoardPins[], uint64_t address, int entry) {
    float currentTemperature;
    int connected;

    DisplayResultsSetEntryMarker(entry, 1);

    //connected = OneWireIsDeviceConnected(numberOfPins, tiBoardPins, address);

    //TiBoardTftEntrySetAddress(line, address, connected);

    //if (connected) {
    //    if (OneWireTemperatureSensorIsAddressOfSensor(address)) {
    connected = OneWireTemperatureSensorGetTemperatureOfSelectedSensor(numberOfPins, tiBoardPins, address, &currentTemperature);

    if (connected)
        DisplayResultsSetEntryTemperature(entry, currentTemperature, DISPLAY_TEMPERATURE_AS_KELVIN);
    //    }
    //}
    DisplayResultsSetEntryMarker(entry, 0);
}

void test(void) {
    TIBOARDPIN pins[] = {
        //        {GPIOG, 2},
        {GPIOG, 1},
        {GPIOG, 0}
    };
    int numberOfPins = sizeof (pins) / sizeof (pins[0]);

    while (1) {
        TFT_gotoxy(1, 1);
        if (OneWireSendReset(numberOfPins, pins))
            TFT_puts("Hello!            ");
        else
            TFT_puts("Anybody out there?");
    }
}

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void) {
    TIBOARDPIN pins[] = {
        {GPIOG, 2},
        {GPIOG, 1},
        {GPIOG, 0}
    };

    int numberOfPins = sizeof (pins) / sizeof (pins[0]);

    ADDRESSLIST* addressList = 0;
    int numberOfEntries = 0;
    ADDRESSLIST* currentAddressListEntry = 0;
    int indexOfCurrentAddress = 0;
    int indexOfLastScannedSensor = 0;

    Init();

    //test();

    while (1) {
        OneWireROMSeach(numberOfPins, pins, &addressList);

        DisplayResultsAddressList(addressList, &numberOfEntries);

        //Wenn keine Adressen gespeichert sind, brauchen von diesen die Temperaturen nicht angezeigt werden.
        if (addressList == 0)
            continue;

        if (currentAddressListEntry == 0) {
            currentAddressListEntry = addressList;
            indexOfCurrentAddress = 0;
        }

        //Nach einem Sensor suchen, der angeschlossen ist
        //Wenn man wieder beim Sensor angekommen ist, der als letztes gescannt wurde, suche abbbrechen
        while (currentAddressListEntry->connected == 0 && indexOfCurrentAddress != indexOfLastScannedSensor) {
            indexOfCurrentAddress++;
            currentAddressListEntry = currentAddressListEntry->next;

            if (currentAddressListEntry == 0) {
                currentAddressListEntry = addressList;
                indexOfCurrentAddress = 0;
            }
        }

        if (currentAddressListEntry->connected) {
            if (OneWireTemperatureSensorIsAddressOfSensor(currentAddressListEntry->address))
                GetTemperaturOfSensorAndDisplay(numberOfPins, pins, currentAddressListEntry->address, indexOfCurrentAddress);
            indexOfLastScannedSensor = indexOfCurrentAddress;
            currentAddressListEntry = currentAddressListEntry->next;
            indexOfCurrentAddress++;
        }
    }
}
