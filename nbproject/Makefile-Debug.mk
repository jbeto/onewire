#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/USER/DisplayResultsHandler.o \
	${OBJECTDIR}/USER/OneWireHandler.o \
	${OBJECTDIR}/USER/OneWireTemperaturSensorHandler.o \
	${OBJECTDIR}/USER/TiBoardHandler.o \
	${OBJECTDIR}/USER/TiBoardTftHandler.o \
	${OBJECTDIR}/USER/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onewire

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onewire: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onewire ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/USER/DisplayResultsHandler.o: USER/DisplayResultsHandler.c 
	${MKDIR} -p ${OBJECTDIR}/USER
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/USER/DisplayResultsHandler.o USER/DisplayResultsHandler.c

${OBJECTDIR}/USER/OneWireHandler.o: USER/OneWireHandler.c 
	${MKDIR} -p ${OBJECTDIR}/USER
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/USER/OneWireHandler.o USER/OneWireHandler.c

${OBJECTDIR}/USER/OneWireTemperaturSensorHandler.o: USER/OneWireTemperaturSensorHandler.c 
	${MKDIR} -p ${OBJECTDIR}/USER
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/USER/OneWireTemperaturSensorHandler.o USER/OneWireTemperaturSensorHandler.c

${OBJECTDIR}/USER/TiBoardHandler.o: USER/TiBoardHandler.c 
	${MKDIR} -p ${OBJECTDIR}/USER
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/USER/TiBoardHandler.o USER/TiBoardHandler.c

${OBJECTDIR}/USER/TiBoardTftHandler.o: USER/TiBoardTftHandler.c 
	${MKDIR} -p ${OBJECTDIR}/USER
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/USER/TiBoardTftHandler.o USER/TiBoardTftHandler.c

${OBJECTDIR}/USER/main.o: USER/main.c 
	${MKDIR} -p ${OBJECTDIR}/USER
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/USER/main.o USER/main.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/onewire

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
